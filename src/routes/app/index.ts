import * as Router from 'koa-router';

const router = new Router();

router.use(function(ctx, next) {
    if (ctx.isAuthenticated()) {
        return next()
    } else {
        ctx.redirect('/')
    }
});

router.get('/', function(ctx) {
    ctx.body = 'Hello App!';
});

export default router;