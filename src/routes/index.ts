import * as Router from 'koa-router';
import appController from './app';
import authController from './auth';

const router = new Router();

router.get('/', async (ctx) => {
    ctx.body = 'Hello World!';
});

router.use('/app', appController.routes());

export default router;
