import * as Koa from 'koa';
import * as morgan from 'koa-morgan';
import * as bodyParser from 'koa-bodyparser';
import * as session from 'koa-session';
import * as passport from 'koa-passport';
import router from './routes';

const app = new Koa();
const config = {
    port: 3000
};

// Setup Key for Session Encryption
app.keys = ['secret'];

// Setup Common Middleware
app.use(morgan('common'));
app.use(bodyParser());
app.use(session({}, app));
app.use(passport.initialize());
app.use(passport.session());

// Include Routes
app.use(router.routes());

app.listen(config.port, () => console.log('Server listening on', config.port));
